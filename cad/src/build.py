import os.path as op

from panel import get_panel_layers
from pcb_model import get_pcb_layers
from tiles import get_tiles_layers
from utils import MaterialEnum, export_dxf
from constants import MATERIALS


PROJECT_PATH = op.dirname(op.dirname(__file__))
BUILD_PATH = op.join(PROJECT_PATH, 'dxf')


class Material(MaterialEnum):

    COVER = MATERIALS['cover']
    THIN = MATERIALS['thin']
    THICK = MATERIALS['thick']


if __name__ == '__main__':
    print('exporting drawings...')

    export_dxf(BUILD_PATH, {
        **get_panel_layers(),
        **get_pcb_layers(),
        **get_tiles_layers()
    })

    print('done')
