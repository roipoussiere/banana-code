# Utils

NUTS_SIZE = {
    'M2': 4.6,
    'M3': 6.3,
    'M4': 8.08,
    'M5': 9.23,
    'M6': 11.54,
    'M8': 15.01
}

# Panel

# MODULES_GRID = 2, 4
MODULES_GRID = 2, 2
MODULE_SIZE = 100, 60
PANEL_SIZE = (
    MODULES_GRID[0] * MODULE_SIZE[0],
    MODULES_GRID[1] * MODULE_SIZE[1]
)

SLOTS_GRID = 2, 4
SLOTS_SPACING = (
    MODULE_SIZE[0] / SLOTS_GRID[0],
    MODULE_SIZE[1] / SLOTS_GRID[1]
)

CABLE_PADS_SPACING = 34
CABLE_PADS_DIAMETER = 8
CABLE_MAGNETS_DIAMETER = 12
CABLE_SCREW_DIAMETER = 4


TILE_PADS_SPACING = 12
TILE_PADS_DIAMETER = 7
TILE_SPRING_DIAMETER = 11

HOLDING_SCREW_DIAM = 2

CMS_AREA_GRID = 2, 1
CMS_AREA_SIZE = 25, 50

## Pcb model

PCB_SIZE = MODULE_SIZE[0] - 2, MODULE_SIZE[1] - 2

# Tile

FINGER_SHAPE_DIAM = 10
METAL_BALL_DIAM = 3
RESISTOR_HOLE_SPACING = 16
RESISTOR_HOLE_SIZE = 2.5, 6.5
# MAGNET_NUT_HEX_DIAG = NUTS_SIZE['M4'] - 0.3
TILES_SIZES = [ 1, 2, 3 ]

# Common

FILLET = 1.5

RENDERING_PARTS_DIST = 5

MATERIALS = {
    'cover': 0.5,
    'thin': 1.5,
    'thick': 4
}
