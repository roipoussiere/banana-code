from abc import ABC, abstractmethod

from utils import LayerEnum


class Drawing(ABC):

    def __init__(self, layer: LayerEnum):
        self.layer = layer

    @abstractmethod
    def contour() -> None:
        pass

    @abstractmethod
    def inner() -> None:
        pass

    @abstractmethod
    def sketch() -> None:
        pass
