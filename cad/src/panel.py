import build123d as b

from drawing import Drawing
from utils import LayerEnum, Layers, sketch, render, export_dxf
from constants import *


class ModuleLayer(LayerEnum):
    BACK   = 1, 'module_back'
    MIDDLE = 2, 'module_middle'
    COVER  = 3, 'module_cover'


class Module(Drawing):
    @sketch
    def contour(self):
        b.Rectangle(*PANEL_SIZE)

    @sketch
    def cable_pads(self):
        diameters = {
            ModuleLayer.COVER: CABLE_PADS_DIAMETER,
            ModuleLayer.MIDDLE: CABLE_MAGNETS_DIAMETER,
            ModuleLayer.BACK: CABLE_SCREW_DIAMETER
        }

        with b.GridLocations(CABLE_PADS_SPACING, 0, 2, 1):
            b.Circle(diameters[self.layer] / 2)

    @sketch
    def tile_pads(self):
        diameters = {
            ModuleLayer.COVER: TILE_PADS_DIAMETER,
            ModuleLayer.MIDDLE: TILE_SPRING_DIAMETER
        }

        with b.GridLocations(TILE_PADS_SPACING, 0, 2, 1):
            b.Circle(diameters[self.layer] / 2)

    @sketch
    def cms_area(self):
        spacing = SLOTS_SPACING[0], 0
        with b.GridLocations(*spacing, *CMS_AREA_GRID):
            b.Rectangle(*CMS_AREA_SIZE)

    @sketch
    def holes(self):
        with b.GridLocations(*SLOTS_SPACING, *SLOTS_GRID):
            b.Add(self.cable_pads())
            if self.layer != ModuleLayer.BACK:
                b.Add(self.tile_pads())

        if self.layer == ModuleLayer.BACK:
            b.Add(self.cms_area())

    @sketch
    def inner(self):
        with b.GridLocations(*MODULE_SIZE, *MODULES_GRID):
            b.Add(self.holes())

    @sketch
    def sketch(self):
        b.Add(self.contour())

        b.Add(self.inner(), mode=b.Mode.SUBTRACT)


def get_panel_layers() -> Layers:
    return { layer: Module(layer).sketch() for layer in ModuleLayer }
    # return { ModuleLayer.COVER: Module(ModuleLayer.COVER).sketch() }

if 'show_object' in locals():
    show_object(render(get_panel_layers(), PANEL_SIZE[1] + RENDERING_PARTS_DIST))

if __name__ == '__main__':
    from build import BUILD_PATH
    export_dxf(BUILD_PATH, get_panel_layers())
