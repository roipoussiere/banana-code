import build123d as b

from drawing import Drawing
from utils import LayerEnum, Layers, sketch, render, export_dxf
from constants import *


class PcbModelLayer(LayerEnum):
    PCB_MODEL = 1, 'pcb_model'


class PcbModel(Drawing):

    @sketch
    def contour(self):
        b.Rectangle(*PCB_SIZE)
        # b.Fillet(*builder.vertices(), radius=FILLET)

    @sketch
    def inner(self):
        b.Circle(HOLDING_SCREW_DIAM / 2)

    @sketch
    def sketch(self):
        b.Add(self.contour())
        b.Add(self.inner(), mode=b.Mode.SUBTRACT)


def get_pcb_layers() -> Layers:
    return { layer: PcbModel(layer).sketch() for layer in PcbModelLayer }

if 'show_object' in locals():
    show_object(render(get_pcb_layers(), PANEL_SIZE[1] + RENDERING_PARTS_DIST))

if __name__ == '__main__':
    from build import BUILD_PATH
    export_dxf(BUILD_PATH, get_pcb_layers())
