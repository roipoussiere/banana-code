import build123d as b

from drawing import Drawing
from utils import LayerEnum, Layers, sketch, render, export_dxf
from constants import *


class TilesLayer(LayerEnum):
    MIDDLE = 1, 'tiles_middle'
    COVER  = 2, 'tiles_cover'


class Tiles(Drawing):

    @classmethod
    def tile_height(self, size: int):
        return size * SLOTS_SPACING[1] - 2

    @sketch
    def tile_contour(self, size: int):
        tile_height = self.tile_height(size)

        b.Rectangle(CABLE_PADS_SPACING - 3.5, tile_height)

        with b.GridLocations(CABLE_PADS_SPACING, SLOTS_SPACING[1], 2, size):
            b.Circle(TILE_PADS_DIAMETER / 2, mode=b.Mode.SUBTRACT)

        with b.GridLocations(0, tile_height, 1, 2):
            finger_size = FINGER_SHAPE_DIAM / 2, FINGER_SHAPE_DIAM / 4
            b.Ellipse(*finger_size, mode=b.Mode.SUBTRACT)

        # with b.GridLocations(CABLE_PADS_SPACING, self.tile_height(size), 2, 2):
        #     b.Rectangle(9, 9, mode=b.Mode.SUBTRACT)

        # b.Fillet(*builder.vertices(), radius=FILLET)

    @sketch
    def tile_hole(self, size: int):
        with b.GridLocations(0, SLOTS_SPACING[1], 1, size):
            b.Circle(METAL_BALL_DIAM / 2)
            # b.RegularPolygon(MAGNET_NUT_HEX_DIAG / 2, 6, rotation=90)

        y_count = size - 1 if size > 1 else 1
        with b.GridLocations(RESISTOR_HOLE_SPACING, SLOTS_SPACING[1], 2, y_count):
            b.Rectangle(*RESISTOR_HOLE_SIZE)

    @sketch
    def tile(self, layer: TilesLayer, size: int):
        b.Add(self.tile_contour(size))
        if layer == TilesLayer.MIDDLE:
            b.Add(self.tile_hole(size), mode=b.Mode.SUBTRACT)

    @sketch
    def contour(self):
        contours = [ self.tile_contour(n) for n in TILES_SIZES ]
        grid = b.GridLocations(CABLE_PADS_SPACING, 0, len(contours), 1)

        for i, l in enumerate(grid.locations):
            with b.Locations(l):
                b.Add(contours[i])

    @sketch
    def inner(self):
        holes = [ self.tile_hole(n) for n in TILES_SIZES ]
        grid = b.GridLocations(CABLE_PADS_SPACING, 0, len(holes), 1)

        for i, l in enumerate(grid.locations):
            with b.Locations(l):
                b.Add(holes[i])

    @sketch
    def sketch(self):
        b.Add(self.contour())
        if self.layer == TilesLayer.MIDDLE:
            b.Add(self.inner(), mode=b.Mode.SUBTRACT)

def get_tiles_layers() -> Layers:
    return { layer: Tiles(layer).sketch() for layer in TilesLayer }

if 'show_object' in locals():
    show_object(render(get_tiles_layers(), MODULE_SIZE[1] + RENDERING_PARTS_DIST))

if __name__ == '__main__':
    from build import BUILD_PATH
    export_dxf(BUILD_PATH, get_tiles_layers())
