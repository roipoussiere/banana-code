import os
import os.path as op
from typing import Dict
from enum import Enum, auto
import functools

from cadquery import Compound, Workplane, exporters
import build123d as b


def line(_func=None, *, workplanes=['XY'], pass_builder=False):

    def decorator(func):

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            with b.BuildLine(*workplanes, mode=b.Mode.PRIVATE) as builder:
                if pass_builder:
                    kwargs['builder'] = builder
                func(*args, **kwargs)
            return builder.line

        return wrapper

    return decorator if _func is None else decorator(_func)


def sketch(_func=None, *, workplanes=['XY'], pass_builder=False):

    def decorator(func):

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            with b.BuildSketch(*workplanes, mode=b.Mode.PRIVATE) as builder:
                if pass_builder:
                    kwargs['builder'] = builder
                func(*args, **kwargs)
            return builder.sketch

        return wrapper

    return decorator if _func is None else decorator(_func)


def part(_func=None, *, workplanes=['XY'], pass_builder=False):

    def decorator(func):

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            with b.BuildPart(*workplanes, mode=b.Mode.PRIVATE) as builder:
                if pass_builder:
                    kwargs['builder'] = builder
                func(*args, **kwargs)
            return builder.part

        return wrapper

    return decorator if _func is None else decorator(_func)


class MaterialEnum(Enum):

    def __init__(self, thickness: float):
        self._value_ = auto()
        self.thickness = thickness


class LayerEnum(Enum):
    def __init__(self, order: int, label: str):
        self.order = order
        self.label = label

    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.order >= other.order
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.order > other.order
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.order <= other.order
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.order < other.order
        return NotImplemented


Layers = Dict[LayerEnum, Compound]


def export_dxf(build_path: str, layers: Layers):
    if not op.isdir(build_path):
        os.makedirs(build_path)

    for layer, sketch in layers.items():
        workplane = Workplane().add(sketch)
        file_path = op.join(build_path, f'{ layer.label }.dxf')
        exporters.export(workplane, file_path)

@sketch
def render(layers: Layers, sep: float):
    layers = list(layers.values())
    grid = b.GridLocations(0, sep, 1, len(layers))

    for i, l in enumerate(grid.locations):
        with b.Locations(l):
            b.Add(layers[i])
