import tkinter as tk
from tkinter import filedialog
import os

root = tk.Tk()
root.withdraw()

default_path = os.path.join('/media', os.getlogin())
default_name = 'baco_22-10-09_22-21-33.svg'
file_path = filedialog.asksaveasfile(
    title='Save svg as',
    mode='w',
    defaultextension=".svg",
    initialdir=default_path,
    initialfile=default_name,
    filetypes=[("SVG drawings", "*.svg")]
)
print(file_path)
