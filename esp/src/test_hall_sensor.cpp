#include "Arduino.h"

#define PIN_SENSOR 13

bool was_near = false;

void setup() {
    pinMode(PIN_SENSOR, INPUT);
    Serial.begin(115200);
    Serial.println("ready");
}

void loop() {
    bool is_near = ! digitalRead(PIN_SENSOR);

    if (is_near && ! was_near) {
        Serial.println("near");
    } else if (! is_near && was_near) {
        Serial.println("far");
    }
    was_near = is_near;
}
